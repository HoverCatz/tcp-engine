# README #

[**Download latest .jar**](https://bitbucket.org/HoverCatz/tcp-engine/downloads/TCPEngine.jar)


**Example code:**

```
import cat.hover.tcp.Packet;
import cat.hover.tcp.Plugin;
import cat.hover.tcp.TCPEngine;

public class Main implements Plugin {
	
	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		
		TCPEngine engine = new TCPEngine(this);
		engine.setDebug(true);
		if (engine.connect("localhost", 43000, true)) {
			
			engine.startReadWrite();
			/* Do whatever you want */
			
			/**
			 *  'type' is just so the server knows what 'type' the packet is.
			 *  sendPacket(type, object);
			 */
			engine.sendPacket(1, "Can object be anything?");
			engine.sendPacket(2, true);
			engine.sendPacket(3, -24);
			engine.sendPacket(4, new Object[] { 1, true, "Hi there" });
			
		} else {
			System.err.println("Could not connect. Is the server up?");
		}
		
	}
	
	@Override
	public void processPacket(Packet packet) {
		
		int type = packet.getType();
		Object object = packet.getObject();
		
		System.out.println("Got type: " + type + ", object: '" + object + "'.");
		
	}
	
	@Override
	public void onError(String error) {
		System.err.println(error);
	}
	
	@Override
	public void onUnknownError() {
		System.err.println("Unknown error, idk wat do. O:");
	}
	
}
```


### Who do I talk to? ###

* HoverCatz (Skype: brandy.harrington.06)
* Other community or team contact