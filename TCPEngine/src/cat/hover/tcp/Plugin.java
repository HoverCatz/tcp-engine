package cat.hover.tcp;

public interface Plugin {
	
	/**
	 * This will execute on all incoming packets, after you call the method TCPEngine.startReadWrite()
	 * @param packet
	 */
	public void processPacket(Packet packet);
	
	/**
	 * This will return a string with any error messages reported.
	 * TCPEngine.debug does NOT need to be TRUE for this to be executed.
	 * @param message
	 */
	public void onError(String message);
	
	/**
	 * This function will fire whenever the connection is broken, by an unknown reason.
	 * I have no idea why and when this will happen. I guess if there is an exception,
	 * and the message is null.
	 */
	public void onUnknownError();
	
}
