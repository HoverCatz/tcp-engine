package cat.hover.tcp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class TCPEngine {
	
	public static void main(String[] args) {
		System.exit(1);
	}
	
	private final Plugin plugin;
	/**
	 * Initialize the engine
	 * @param plugin
	 */
	public TCPEngine(Plugin plugin) {
		if (plugin == null) throw new IllegalStateException("Object not initialised");
		this.plugin = plugin;
	}
	
	private boolean debug = false, stillConnected = false;
	private Socket socket;
	private DataOutputStream writer;
	private DataInputStream reader;
	
	/**
	 * Create a connection to the server
	 * @param ip
	 * @param port
	 * @param setTcpNoDelay
	 * @return
	 */
	public boolean connect(final String ip, final int port, final boolean setTcpNoDelay) {
		try {
			
			socket = new Socket(ip, port);
			socket.setTcpNoDelay(setTcpNoDelay);
			
			writer = new DataOutputStream(socket.getOutputStream());
			reader = new DataInputStream(socket.getInputStream());
			
			stillConnected = true;
			return true;
		} catch (Exception e) {
			if (e.getMessage() != null) {
				plugin.onError(e.getMessage());
			} else {
				plugin.onUnknownError();
			}
			if (debug && e != null) e.printStackTrace();
			stillConnected = false;
		}
		return false;
	}
	
	/**
	 * Start reading for incoming packets, and
	 * Start writing outgoing packets
	 */
	public void startReadWrite() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				listen();
			}
		}).start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				write();
			}
		}).start();
	}
	private void listen() {
		while (stillConnected) {
			try {
				
				int len = reader.readInt();
				byte[] bytes = new byte[len];
				Packet packet = getPacketFromBytes(bytes);
				
				plugin.processPacket(packet);
				
			} catch (Exception e) {
				if (e.getMessage() != null) {
					plugin.onError("Something interrupted the listen function: " + e.getMessage());
				} else {
					plugin.onUnknownError();
				}
				if (debug && e != null) e.printStackTrace();
				stillConnected = false;
			}
		}
	}
	private ConcurrentLinkedQueue<Packet> packetsToBeSent = new ConcurrentLinkedQueue<Packet>();
	private void write() {
		while (stillConnected) {
			try {
				
				if (!packetsToBeSent.isEmpty()) {
					Packet packet = packetsToBeSent.poll();
					if (packet != null) {
						byte[] bytes = getBytesFromPacket(packet);
						writer.writeInt(bytes.length);
						writer.write(bytes, 0, bytes.length);
						writer.flush();
					}
				} else {
					Thread.sleep(1);
				}
				
			} catch (Exception e) {
				if (e.getMessage() != null) {
					plugin.onError("Something interrupted the write function: " + e.getMessage());
				} else {
					plugin.onUnknownError();
				}
				if (debug && e != null) e.printStackTrace();
				stillConnected = false;
			}
		}
	}
	
	private Packet getPacketFromBytes(byte[] bytes) throws Exception {
		reader.readFully(bytes, 0, bytes.length);
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		ObjectInput in = new ObjectInputStream(bis);
		Object[] object = (Object[])in.readObject();
		Packet packet = new Packet((int)object[0], (Object)object[1]);
		return packet;
	}
	private byte[] getBytesFromPacket(Packet packet) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(new Object[] { packet.getType(), packet.getObject() });
		byte[] bytes = baos.toByteArray();
		oos.close();
		return bytes;
	}
	
	/**
	 * Adds a packet to the send-queue.
	 * Will be sent almost instantly on another thread.
	 * @param name
	 * @param object
	 * @throws IOException 
	 */
	public void sendPacket(int type, Object object){
		Packet packet = new Packet(type, object);
		packetsToBeSent.add(packet);
	}
	
	/**
	 * Set the debug parameter (TRUE / FALSE). FALSE by default.
	 * @param debug
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
}
