package cat.hover.tcp;



import java.io.Serializable;

public class Packet implements Serializable {
	private static final long serialVersionUID = -1070851220139414687L;
	
	private int type;
	private Object object;
	public Packet(int type, Object object) {
		this.type = type;
		this.object = object;
	}
	
	public int getType() {
		return this.type;
	}
	public Object getObject() {
		return this.object;
	}
	
}
